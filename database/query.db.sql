// Consulta simples retornando todos os pedidos
SELECT * FROM pedidos;

// Consulta com cláusula WHERE filtrando por status
SELECT * FROM pedidos 
WHERE status = 'FINALIZADO';

// Consulta com JOIN entre tabelas
SELECT p.id, c.nome as cliente, p.status, p.data_hora  
FROM pedidos p
JOIN clientes c ON c.id = p.cliente_id;

// Consulta com GROUP BY e HAVING
SELECT cliente_id, COUNT(*) AS qtde_pedidos
FROM pedidos
GROUP BY cliente_id
HAVING COUNT(*) > 1;

// Subconsulta na cláusula WHERE
SELECT * FROM pedidos
WHERE cliente_id IN (
  SELECT id FROM clientes WHERE nome LIKE 'João'
);

// Subconsulta no SELECT
SELECT p.*,
  (SELECT nome FROM clientes c WHERE c.id = p.cliente_id) AS nome_cliente
FROM pedidos p;

// UNION combinando resultados de consultas 
SELECT id, nome AS nome_peca, 'Pecas' AS tipo 
FROM pecas_roupa
UNION 
SELECT id, nome AS nome_cliente, 'Clientes' AS tipo
FROM clientes;

// Consulta com CTE (Common Table Expression)
WITH pedidos_finalizados AS (
  SELECT * FROM pedidos WHERE status = 'FINALIZADO'  
)
SELECT * FROM pedidos_finalizados;

// Consulta com dados agrupados e totalizados
SELECT
EXTRACT(YEAR FROM data_hora) AS ano,
EXTRACT(MONTH FROM data_hora) AS mes,
COUNT(*) AS total_pedidos,
SUM(ip.quantidade * pr.preco) AS faturamento
FROM pedidos p
JOIN itens_pedido ip ON ip.pedido_id = p.id

JOIN pecas_roupa pr ON pr.id = ip.peca_id
GROUP BY 1, 2
ORDER BY 1, 2;