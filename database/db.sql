Aqui está a versão dos SQLs para PostgreSQL:

CREATE TABLE usuarios (
  id SERIAL PRIMARY KEY,
  nome VARCHAR(100) NOT NULL,
  email VARCHAR(100) UNIQUE NOT NULL,
  senha VARCHAR(100) NOT NULL,
  perfil VARCHAR(20) NOT NULL CHECK (perfil IN ('CLIENTE', 'FUNCIONARIO'))
);

CREATE TABLE clientes (
  id SERIAL PRIMARY KEY,
  cpf CHAR(11) UNIQUE NOT NULL,
  nome VARCHAR(100) NOT NULL,
  email VARCHAR(100) UNIQUE NOT NULL,
  telefone VARCHAR(20) NOT NULL,
  cep VARCHAR(8) NOT NULL,
  endereco VARCHAR(200) NOT NULL,
  numero VARCHAR(20) NOT NULL,
  complemento VARCHAR(100),
  bairro VARCHAR(100) NOT NULL,
  cidade VARCHAR(100) NOT NULL,
  estado CHAR(2) NOT NULL
);

CREATE TABLE funcionarios (
  id SERIAL PRIMARY KEY,
  nome VARCHAR(100) NOT NULL,
  email VARCHAR(100) UNIQUE NOT NULL,
  data_nascimento DATE NOT NULL,
  senha VARCHAR(100) NOT NULL
);

CREATE TABLE pecas_roupa (
  id SERIAL PRIMARY KEY,
  nome VARCHAR(50) NOT NULL,
  preco DECIMAL(10,2) NOT NULL,
  tempo_lavagem INT NOT NULL
);

CREATE TABLE pedidos (
  id SERIAL PRIMARY KEY,
  cliente_id INT NOT NULL,
  data_hora TIMESTAMP NOT NULL,
  status VARCHAR(20) NOT NULL CHECK (status IN ('EM_ABERTO', 'CANCELADO', 'REJEITADO', 'RECOLHIDO',
                                               'AGUARDANDO_PAGAMENTO', 'PAGO', 'FINALIZADO')),
  FOREIGN KEY (cliente_id) REFERENCES clientes(id)  
);

CREATE TABLE itens_pedido (
  pedido_id INT NOT NULL,
  peca_id INT NOT NULL,
  quantidade INT NOT NULL,
  PRIMARY KEY (pedido_id, peca_id),
  FOREIGN KEY (pedido_id) REFERENCES pedidos(id),
  FOREIGN KEY (peca_id) REFERENCES pecas_roupa(id)
);

INSERT INTO usuarios (nome, email, senha, perfil)
VALUES
  ('Maria', 'maria@email.com', '123', 'FUNCIONARIO'),
  ('Mario', 'mario@email.com', '123', 'FUNCIONARIO'),
  ('João', 'joao@email.com', '123', 'CLIENTE'),
  ('José', 'jose@email.com', '123', 'CLIENTE'),
  ('Joana', 'joana@email.com', '123', 'CLIENTE'),
  ('Joaquina', 'joaquina@email.com', '123', 'CLIENTE');

INSERT INTO clientes (cpf, nome, email, telefone, cep, endereco, numero, bairro, cidade, estado)
VALUES
  ('11111111111', 'João', 'joao@email.com', '999999999', '12345678', 'Rua 1', '123', 'Bairro 1', 'Cidade 1', 'UF'),
  ('22222222222', 'José', 'jose@email.com', '999999999', '87654321', 'Rua 2', '123', 'Bairro 2', 'Cidade 2', 'UF'),
  ('33333333333', 'Joana', 'joana@email.com', '999999999', '87654321', 'Rua 3', '123', 'Bairro 3', 'Cidade 3', 'UF'),
  ('44444444444', 'Joaquina', 'joaquina@email.com', '999999999', '87654321', 'Rua 4', '123', 'Bairro 4', 'Cidade 4', 'UF');

INSERT INTO funcionarios (nome, email, data_nascimento, senha)
VALUES
  ('Maria', 'maria@email.com', '1990-01-01', '123'),
  ('Mario', 'mario@email.com', '1990-01-01', '123');

INSERT INTO pecas_roupa (nome, preco, tempo_lavagem)  
VALUES
  ('Calça', 10, 2),
  ('Camisa', 5, 1), 
  ('Camiseta', 3, 1),
  ('Meia', 1, 1),
  ('Cueca', 2, 1);

INSERT INTO pedidos (cliente_id, data_hora, status)
VALUES
  (1, '2023-01-01 10:00:00', 'EM_ABERTO'),
  (1, '2023-01-02 10:00:00', 'EM_ABERTO'),
  (1, '2023-01-03 10:00:00', 'EM_ABERTO'),
  (1, '2023-01-04 10:00:00', 'REJEITADO'),
  (1, '2023-01-05 10:00:00', 'CANCELADO'),
  (1, '2023-01-06 10:00:00', 'RECOLHIDO'),
  (1, '2023-01-07 10:00:00', 'RECOLHIDO'), 
  (1, '2023-01-08 10:00:00', 'FINALIZADO'),
  (1, '2023-01-09 10:00:00', 'FINALIZADO'),

  (2, '2023-01-10 10:00:00', 'EM_ABERTO'),
  (2, '2023-01-11 10:00:00', 'AGUARDANDO_PAGAMENTO'),
  (2, '2023-01-12 10:00:00', 'AGUARDANDO_PAGAMENTO'),
  (2, '2023-01-13 10:00:00', 'PAGO'),
  (2, '2023-01-14 10:00:00', 'FINALIZADO'),
  (2, '2023-01-15 10:00:00', 'FINALIZADO'),

  (3, '2023-01-16 10:00:00', 'RECOLHIDO'),
  (3, '2023-01-17 10:00:00', 'AGUARDANDO_PAGAMENTO'),
  (3, '2023-01-18 10:00:00', 'AGUARDANDO_PAGAMENTO'),
  (3, '2023-01-19 10:00:00', 'PAGO'),
  (3, '2023-01-20 10:00:00', 'FINALIZADO'), 
  (3, '2023-01-21 10:00:00', 'FINALIZADO'),
  (3, '2023-01-22 10:00:00', 'FINALIZADO'),

  (4, '2023-01-23 10:00:00', 'RECOLHIDO'),
  (4, '2023-01-24 10:00:00', 'RECOLHIDO'),
  (4, '2023-01-25 10:00:00', 'AGUARDANDO_PAGAMENTO'),
  (4, '2023-01-26 10:00:00', 'PAGO'),
  (4, '2023-01-27 10:00:00', 'FINALIZADO'),
  (4, '2023-01-28 10:00:00', 'FINALIZADO');

INSERT INTO itens_pedido (pedido_id, peca_id, quantidade)
VALUES
  (1, 1, 2),
  (1, 2, 1),
  (2, 3, 3), 
  (3, 4, 5),
  (4, 5, 2);